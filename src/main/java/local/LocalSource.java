package local;

import local.models.CPU;
import local.models.GPU;
import org.hibernate.SessionFactory;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Objects;

public class LocalSource {

    private static LocalSource instance = new LocalSource();
    private SessionFactory factory = HibernateSessionFactoryUtil.getFactory();

    private LocalSource() {
    }

    public static LocalSource getInstance() {
        return instance;
    }

    public ArrayList<CPU> getCPUs() {
        var session = factory.openSession();
        var criteria = session.createCriteria(CPU.class);
        var result = new ArrayList<CPU>(criteria.list());
        session.close();
        return result;
    }

    public long getCPUUpdateTime() {
        var session = factory.openSession();
        var result = new ArrayList<Object>(session.createNativeQuery("SELECT UPDATE_TIME FROM information_schema.TABLES WHERE TABLE_NAME = 'CPUs'").list());
        result.removeIf(Objects::isNull);
        long ans;
        if (result.size() < 1) {
            ans = ((Timestamp) session.createNativeQuery("SELECT CREATE_TIME FROM information_schema.TABLES WHERE TABLE_NAME = 'CPUs'").list().get(0)).getTime();
        } else {
            ans = ((Timestamp) result.get(0)).getTime();
        }
        session.close();
        return ans;
    }

    public long getGPUUpdateTime() {
        var session = factory.openSession();
        var result = new ArrayList<Object>(session.createNativeQuery("SELECT UPDATE_TIME FROM information_schema.TABLES WHERE TABLE_NAME = 'GPUs'").list());
        result.removeIf(Objects::isNull);
        long ans;
        if (result.size() < 1) {
            ans = ((Timestamp) session.createNativeQuery("SELECT CREATE_TIME FROM information_schema.TABLES WHERE TABLE_NAME = 'GPUs'").list().get(0)).getTime();
        } else {
            ans = ((Timestamp) result.get(0)).getTime();
        }
        session.close();
        return ans;
    }

    public ArrayList<GPU> getGPUs() {
        var session = factory.openSession();
        var criteria = session.createCriteria(GPU.class);
        var result = new ArrayList<GPU>(criteria.list());
        session.close();
        return result;
    }

}
