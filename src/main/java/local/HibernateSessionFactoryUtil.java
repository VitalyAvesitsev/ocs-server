package local;

import local.models.CPU;
import local.models.GPU;
import local.models.Socket;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateSessionFactoryUtil {

    private static SessionFactory factory;

    public static SessionFactory getFactory() {
        try {
            if (factory == null) {
                var config = new Configuration()
                        .configure()
                        .addAnnotatedClass(Socket.class)
                        .addAnnotatedClass(CPU.class)
                        .addAnnotatedClass(GPU.class);
                factory = config.buildSessionFactory(
                        new StandardServiceRegistryBuilder()
                                .applySettings(config.getProperties())
                                .build()
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return factory;
    }

}
