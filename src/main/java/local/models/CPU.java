package local.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "OCS.CPUs")
public class CPU {

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "family")
    private String family;

    @Column(name = "generation")
    private String generation;

    @Column(name = "type")
    private String type;

    @Column(name = "cores")
    private Integer cores;

    @Column(name = "threads")
    private Integer threads;

    @Column(name = "base_clock")
    private String baseClock;

    @Column(name = "turbo_clock")
    private String turboClock;

    @Column(name = "tdp")
    private Double tdp;

    @Column(name = "launch_date")
    private String launchDate;

    @Column(name = "lithography")
    private String lithography;

    @Column(name = "sockets")
    private String sockets;

    @Column(name = "surface")
    private String surface;

    @Column(name = "size")
    private String size;

    @Column(name = "transistors")
    private String transistors;

    @Column(name = "embedded")
    private String embedded;

    @Column(name = "cache")
    private String cache;

    @Column(name = "scale")
    private Integer scale;

    @Column(name = "instruction_set")
    private String instructionSet;

    @Column(name = "ext_instructions")
    private String extInstructions;

    @Column(name = "core_number")
    private String coreNumber;

    @Column(name = "ecc")
    private String ecc;

    @Column(name = "max_ram")
    private String maxRam;

    @Column(name = "ram_types")
    private String ramTypes;

    @Column(name = "ram_channels")
    private Integer ramChannels;

    @Column(name = "ram_clock")
    private String ramClock;

    @Column(name = "ram_bandwidth")
    private String ramBandwidth;

    @Column(name = "t_junction")
    private String tJunction;

    @Column(name = "pcie_type")
    private String pcieType;

    @Column(name = "pcie_config")
    private String pcieConfig;

    @Column(name = "pcie_count")
    private Integer pcieCount;

    @Column(name = "vendor")
    private String vendor;

//----------------------------------------------------------------------------------------------------------------------

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return this.family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getGeneration() {
        return this.generation;
    }

    public void setGeneration(String generation) {
        this.generation = generation;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getCores() {
        return this.cores;
    }

    public void setCores(Integer cores) {
        this.cores = cores;
    }

    public Integer getThreads() {
        return this.threads;
    }

    public void setThreads(Integer threads) {
        this.threads = threads;
    }

    public String getBaseClock() {
        return this.baseClock;
    }

    public void setBaseClock(String baseClock) {
        this.baseClock = baseClock;
    }

    public String getTurboClock() {
        return this.turboClock;
    }

    public void setTurboClock(String turboClock) {
        this.turboClock = turboClock;
    }

    public Double getTdp() {
        return this.tdp;
    }

    public void setTdp(Double tdp) {
        this.tdp = tdp;
    }

    public String getLaunchDate() {
        return this.launchDate;
    }

    public void setLaunchDate(String launchDate) {
        this.launchDate = launchDate;
    }

    public String getLithography() {
        return this.lithography;
    }

    public void setLithography(String lithography) {
        this.lithography = lithography;
    }

    public String getSockets() {
        return this.sockets;
    }

    public void setSockets(String sockets) {
        this.sockets = sockets;
    }

    public String getSurface() {
        return this.surface;
    }

    public void setSurface(String surface) {
        this.surface = surface;
    }

    public String getSize() {
        return this.size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getTransistors() {
        return this.transistors;
    }

    public void setTransistors(String transistors) {
        this.transistors = transistors;
    }

    public String getEmbedded() {
        return this.embedded;
    }

    public void setEmbedded(String embedded) {
        this.embedded = embedded;
    }

    public String getCache() {
        return this.cache;
    }

    public void setCache(String cache) {
        this.cache = cache;
    }

    public Integer getScale() {
        return this.scale;
    }

    public void setScale(Integer scale) {
        this.scale = scale;
    }

    public String getInstructionSet() {
        return this.instructionSet;
    }

    public void setInstructionSet(String instructionSet) {
        this.instructionSet = instructionSet;
    }

    public String getExtInstructions() {
        return this.extInstructions;
    }

    public void setExtInstructions(String extInstructions) {
        this.extInstructions = extInstructions;
    }

    public String getCoreNumber() {
        return this.coreNumber;
    }

    public void setCoreNumber(String coreNumber) {
        this.coreNumber = coreNumber;
    }

    public String getEcc() {
        return this.ecc;
    }

    public void setEcc(String ecc) {
        this.ecc = ecc;
    }

    public String getMaxRam() {
        return this.maxRam;
    }

    public void setMaxRam(String maxRam) {
        this.maxRam = maxRam;
    }

    public String getRamTypes() {
        return this.ramTypes;
    }

    public void setRamTypes(String ramTypes) {
        this.ramTypes = ramTypes;
    }

    public Integer getRamChannels() {
        return this.ramChannels;
    }

    public void setRamChannels(Integer ramChannels) {
        this.ramChannels = ramChannels;
    }

    public String getRamClock() {
        return this.ramClock;
    }

    public void setRamClock(String ramClock) {
        this.ramClock = ramClock;
    }

    public String getRamBandwidth() {
        return this.ramBandwidth;
    }

    public void setRamBandwidth(String ramBandwidth) {
        this.ramBandwidth = ramBandwidth;
    }

    public String getTJunction() {
        return this.tJunction;
    }

    public void setTJunction(String tJunction) {
        this.tJunction = tJunction;
    }

    public String getPcieType() {
        return this.pcieType;
    }

    public void setPcieType(String pcieType) {
        this.pcieType = pcieType;
    }

    public String getPcieConfig() {
        return this.pcieConfig;
    }

    public void setPcieConfig(String pcieConfig) {
        this.pcieConfig = pcieConfig;
    }

    public Integer getPcieCount() {
        return this.pcieCount;
    }

    public void setPcieCount(Integer pcieCount) {
        this.pcieCount = pcieCount;
    }

    public String getVendor() {
        return this.vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String gettJunction() {
        return tJunction;
    }

    public void settJunction(String tJunction) {
        this.tJunction = tJunction;
    }
}
