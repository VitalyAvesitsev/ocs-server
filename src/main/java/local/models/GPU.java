package local.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GPUs")
public class GPU {
    @Id
    @Column(name = "name")
    private String name;

    @Column(name = "launch")
    private String launch;

    @Column(name = "codename")
    private String codename;

    @Column(name = "lithography")
    private String lithography;

    @Column(name = "transistors")
    private Double transistors;

    @Column(name = "die_size")
    private Integer dieSize;

    @Column(name = "interfaces")
    private String interfaces;

    @Column(name = "L2")
    private Double l2;

    @Column(name = "base_clock")
    private String baseClock;

    @Column(name = "turbo_clock")
    private String turboClock;

    @Column(name = "pixel_filtrate")
    private String pixelFiltrate;

    @Column(name = "texture_filtrate")
    private String textureFiltrate;

    @Column(name = "vram")
    private Integer vram;

    @Column(name = "vram_bandwidth")
    private String vramBandwidth;

    @Column(name = "vram_type")
    private String vramType;

    @Column(name = "vram_bus")
    private Integer vramBus;

    @Column(name = "direct_x")
    private String directX;

    @Column(name = "OpenGL")
    private String openGl;

    @Column(name = "tdp")
    private Double tdp;

    @Column(name = "multigpu")
    private String multigpu;

//----------------------------------------------------------------------------------------------------------------------

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLaunch() {
        return this.launch;
    }

    public void setLaunch(String launch) {
        this.launch = launch;
    }

    public String getCodename() {
        return this.codename;
    }

    public void setCodename(String codename) {
        this.codename = codename;
    }

    public String getLithography() {
        return this.lithography;
    }

    public void setLithography(String lithography) {
        this.lithography = lithography;
    }

    public Double getTransistors() {
        return this.transistors;
    }

    public void setTransistors(Double transistors) {
        this.transistors = transistors;
    }

    public Integer getDieSize() {
        return this.dieSize;
    }

    public void setDieSize(Integer dieSize) {
        this.dieSize = dieSize;
    }

    public String getInterfaces() {
        return this.interfaces;
    }

    public void setInterfaces(String interfaces) {
        this.interfaces = interfaces;
    }

    public Double getL2() {
        return this.l2;
    }

    public void setL2(Double l2) {
        this.l2 = l2;
    }

    public String getBaseClock() {
        return this.baseClock;
    }

    public void setBaseClock(String baseClock) {
        this.baseClock = baseClock;
    }

    public String getTurboClock() {
        return this.turboClock;
    }

    public void setTurboClock(String turboClock) {
        this.turboClock = turboClock;
    }

    public String getPixelFiltrate() {
        return this.pixelFiltrate;
    }

    public void setPixelFiltrate(String pixelFiltrate) {
        this.pixelFiltrate = pixelFiltrate;
    }

    public String getTextureFiltrate() {
        return this.textureFiltrate;
    }

    public void setTextureFiltrate(String textureFiltrate) {
        this.textureFiltrate = textureFiltrate;
    }

    public Integer getVram() {
        return this.vram;
    }

    public void setVram(Integer vram) {
        this.vram = vram;
    }

    public String getVramBandwidth() {
        return this.vramBandwidth;
    }

    public void setVramBandwidth(String vramBandwidth) {
        this.vramBandwidth = vramBandwidth;
    }

    public String getVramType() {
        return this.vramType;
    }

    public void setVramType(String vramType) {
        this.vramType = vramType;
    }

    public Integer getVramBus() {
        return this.vramBus;
    }

    public void setVramBus(Integer vramBus) {
        this.vramBus = vramBus;
    }

    public String getDirectX() {
        return this.directX;
    }

    public void setDirectX(String directX) {
        this.directX = directX;
    }

    public String getOpenGl() {
        return this.openGl;
    }

    public void setOpenGl(String openGl) {
        this.openGl = openGl;
    }

    public Double getTdp() {
        return this.tdp;
    }

    public void setTdp(Double tdp) {
        this.tdp = tdp;
    }

    public String getMultigpu() {
        return this.multigpu;
    }

    public void setMultigpu(String multigpu) {
        this.multigpu = multigpu;
    }
}
