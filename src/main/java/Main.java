import local.HibernateSessionFactoryUtil;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList<Object> result = new ArrayList<Object>(
                HibernateSessionFactoryUtil
                        .getFactory()
                        .openSession()
                        .createNativeQuery("SELECT UPDATE_TIME FROM information_schema.TABLES WHERE TABLE_NAME = 'intel_cpus'")
                        .list());
        System.out.println(result.toString());
    }

}
