package remote.modules;

import local.LocalSource;
import local.models.CPU;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;

@Path("/cpu")
public class CPUModule {

    @GET
    @Path("/load")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<CPU> loadCPUs() {
        return LocalSource.getInstance().getCPUs();
    }

}
