package remote.modules;

import remote.models.TestModel;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/test")
public class TestModule {

    @GET
    @Path("/getTest")
    @Produces(MediaType.APPLICATION_JSON)
    public TestModel getTest() {
        return new TestModel();
    }

}
