package remote.modules;

import local.LocalSource;
import remote.models.TimeModel;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/general")
public class GeneralModule {

    @GET
    @Path("/updateTimes")
    @Produces(MediaType.APPLICATION_JSON)
    public TimeModel getUpdateTimes() {
        var cpu = LocalSource.getInstance().getCPUUpdateTime();
        var gpu = LocalSource.getInstance().getGPUUpdateTime();
        return new TimeModel(cpu, gpu);
    }


}
