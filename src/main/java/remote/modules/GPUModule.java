package remote.modules;

import local.LocalSource;
import local.models.GPU;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;

@Path("/gpu")
public class GPUModule {

    @GET
    @Path("/list")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<GPU> loadGPUs() {
        return LocalSource.getInstance().getGPUs();
    }


}
