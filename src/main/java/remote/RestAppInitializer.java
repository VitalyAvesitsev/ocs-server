package remote;

import remote.modules.CPUModule;
import remote.modules.GPUModule;
import remote.modules.GeneralModule;
import remote.modules.TestModule;

import javax.ws.rs.core.Application;
import java.util.Set;

public class RestAppInitializer extends Application {

    private Set<Object> modules = Set.of(
            new TestModule(),
            new CPUModule(),
            new GPUModule(),
            new GeneralModule()
    );

    @Override
    public Set<Object> getSingletons() {
        return modules;
    }
}
