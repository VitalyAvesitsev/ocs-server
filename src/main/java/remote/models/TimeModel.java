package remote.models;

public class TimeModel {

    public final long cpu, gpu;

    public TimeModel(long cpu, long gpu) {
        this.cpu = cpu;
        this.gpu = gpu;
    }

}
