package remote.models;

import java.io.Serializable;

public class TestModel implements Serializable {

    private String name = "NAME";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
